import Vue from 'vue'
import App from './App'
import router from './router'
import iView from 'iview';
import 'iview/dist/styles/iview.css'
import VueTheMask from 'vue-the-mask'
import money from 'v-money'

Vue.config.productionTip = false
Vue.use(iView)
Vue.use(VueTheMask)
Vue.use(money)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
