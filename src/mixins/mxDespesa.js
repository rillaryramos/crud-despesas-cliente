export const mxDespesa = {
  data() {

    const validadeValor = (rule, value, callback) => {
        if (this.despesa.valor === 'R$ 0,00') {
            callback(new Error('Por favor, informe o valor'));
        } else {
            callback();
        }
    };

    const validateData = (rule, value, callback) => {
        if (this.validarData(this.despesa.data) == false) {
            callback(new Error('Por favor, informe uma data válida'));
        } else {
            callback();
        }
    };

    const validadeTipoDespesa = (rule, value, callback) => {
        if (this.despesa.tipoDespesa === null) {
            callback(new Error('Por favor, selecione o tipo de despesa'));
        } else {
            callback();
        }
    };

    const validadeFormaPagamento = (rule, value, callback) => {
        if (this.despesa.formaPagamento === null) {
            callback(new Error('Por favor, selecione a forma de pagamento'));
        } else {
            callback();
        }
    };

    const validadeTipoReembolso = (rule, value, callback) => {
        if (this.despesa.tipoReembolso === null) {
            callback(new Error('Por favor, selecione o tipo de reembolso'));
        } else {
            callback();
        }
    };

    const validadeProjeto = (rule, value, callback) => {
        if (this.despesa.projeto === null) {
            callback(new Error('Por favor, selecione o projeto'));
        } else {
            callback();
        }
    };

    const validadeCliente = (rule, value, callback) => {
        if (this.despesa.cliente === null) {
            callback(new Error('Por favor, selecione o cliente'));
        } else {
            callback();
        }
    };

    return {

        ruleValidate: {
            valor: [
                { required: true, validator: validadeValor, trigger: "blur" }
            ],
            data: [
                { required: true, message: "Por favor, informe a data", trigger: "blur" },
                { required: true, validator: validateData,  trigger: "blur" }
            ],
            tipoDespesa: [
                { required: true, validator: validadeTipoDespesa, trigger: 'change' }
            ],
            formaPagamento: [
                { required: true, validator: validadeFormaPagamento, trigger: 'change' }
            ],
            tipoReembolso: [
                { required: true, validator: validadeTipoReembolso, trigger: 'change' }
            ],
            projeto: [
                { required: true, validator: validadeProjeto, trigger: 'change' }
            ],
            cliente: [
                { required: true, validator: validadeCliente, trigger: 'change' }
            ]
        }
    }
  },

  methods: {
    validarData(valor) {
        var date=valor;
        var ardt=new Array;
        var ExpReg=new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");
        ardt=date.split("/");
        var erro=false;

        if (date.search(ExpReg)==-1){
          erro = true;
        }
        else if (((ardt[1]==4)||(ardt[1]==6)||(ardt[1]==9)||(ardt[1]==11))&&(ardt[0]>30))
          erro = true;
        else if ( ardt[1]==2) {
          if ((ardt[0]>28)&&((ardt[2]%4)!=0))
            erro = true;
          if ((ardt[0]>29)&&((ardt[2]%4)==0))
            erro = true;
        }
        if (erro) {
          return false;
        }
        return true;
    }
  }
}
