import Vue from 'vue'
import Router from 'vue-router'
import CadastroDespesa from '@/components/CadastroDespesa'
import Despesas from '@/components/Despesas'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Despesas',
      component: Despesas
    },
    {
      path: '/nova-despesa',
      name: 'cadastroDespesa',
      component: CadastroDespesa
    },
    {
      path: '/editar-despesa/:id',
      name: 'editarDespesa',
      component: CadastroDespesa,
      props: true
    }
  ]
})
